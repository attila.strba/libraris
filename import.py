import csv, sqlite3, time, sys, subprocess, os, shutil
from collections import defaultdict

tables = ['kartoteka', 'jazyk', 'tematika', 'subtematika']
    
    
def exportFromMDB(database):
    
    
    OPTIONS="-H -D %y-%m-%d"
    
    for tableExport in tables:
        f = open(tableExport + ".csv", "w")
        print "Export from ", database, "table " + tableExport + "to " + tableExport + ".csv"
        subprocess.call(["mdb-export",  OPTIONS, database, tableExport], stdout=f) 
        f.close()
    
def prepareSqLite():
    
    cur.execute('SELECT SQLITE_VERSION()')
    data = cur.fetchone()
    print "SQLite version: %s" % data  
    #exit(1)
    cur.execute('CREATE TABLE opts (opt,val)')
    cur.execute('CREATE UNIQUE INDEX idx_opt ON opts(opt)')
    cur.execute('INSERT INTO opts VALUES (\'dbversion\', 3)')
    cur.execute('CREATE TABLE IF NOT EXISTS kartoteka (prirastkove_cislo INTEGER PRIMARY KEY,' 
							'tematika TEXT,'
							'subtematika TEXT,'
							'jazyk TEXT,'
							'nazov TEXT,'
							'autor TEXT,'
							'signatura TEXT,'
							'miesto_vydania TEXT,'
							'rok_vydania TEXT,'
                            'vydavatelstvo TEXT,'
							'obsah TEXT)')



if __name__ == "__main__":

    t = time.time()
    
    exportFromMDB("libraris-final2.mdb")
    
    try:
        os.remove("kalaka.sqlite3")
    except OSError:
        print "No need to remove, no database found"
    
    conn = sqlite3.connect( "kalaka.sqlite3" )
    conn.text_factory = str  #bugger 8-bit bytestrings
    cur = conn.cursor()
    prepareSqLite()

    #create dictionaries from the csv tables
    
    dictTematika = {}
    dictJazyk = {}
    dictSubtematika = {}
    
    print tables
    #tematika
    csvFile = csv.reader(open(tables[2] + '.csv', "rb"),delimiter=",")
    for field in csvFile:
        dictTematika[field[0]] = field[1]

    #jazyk
    csvFile = csv.reader(open(tables[1] + '.csv', "rb"),delimiter=",")
    for field in csvFile:
        dictJazyk[field[0]] = field[1]

    #subtematika        
    csvFile = csv.reader(open(tables[3] + '.csv', "rb"),delimiter=",")
    for field in csvFile:
        dictSubtematika[field[0]] = field[2]

    print dictTematika 
    print dictJazyk 
    print dictSubtematika 
    #os.exit(0)

    csvData = csv.reader(open('kartoteka.csv', "rb"),delimiter=",")
    
    for chunk in csvData:
 
        try:     
            tematika = dictTematika[chunk[2]]            
            #print tematika
        except KeyError: 
            tematika = ''

        try:     
            subtematika = dictSubtematika[chunk[3]]            
            #print chunk[3],subtematika
        except KeyError: 
            subtematika = ''

        try:     
            jazyk = dictJazyk[chunk[4]]            
            #print jazyk
        except KeyError: 
            jazyk = ''

            
        cur.execute('INSERT OR IGNORE INTO kartoteka (prirastkove_cislo, tematika, subtematika, jazyk, nazov, autor, signatura, miesto_vydania, rok_vydania, vydavatelstvo, obsah) VALUES (?,?,?,?,?,?,?,?,?,?,?)', (chunk[0], \
            tematika, subtematika, jazyk, chunk[5], chunk[6], chunk[7], chunk[8], chunk[9], chunk[10], chunk[13]))
        print (chunk[0], tematika, subtematika, jazyk)
 
    conn.commit()
    cur.close()
    conn.close()
    print "\n Time Taken: %.3f sec\nfile copied to:/var/www/bibliothekakalaka.org/html/data/meta/kalaka.sqlite3 " % (time.time()-t) 
    shutil.copyfile("kalaka.sqlite3", "/var/www/bibliothekakalaka.org/html/data/meta/kalaka.sqlite3")

